<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rodller
 */
?>

<?php get_template_part('template-parts/ads/before-footer'); ?>

<div id="rodller-prefooter">
	<?php get_template_part('template-parts/footer/pre'); ?>
</div>

<?php $footer_columns = rodller_get_footer_columns(); ?>
<?php if(!empty($footer_columns) && (
		is_active_sidebar('rodller-footer-column-sidebar-1') || is_active_sidebar('rodller-footer-column-sidebar-2') ||
		is_active_sidebar('rodller-footer-column-sidebar-3') || is_active_sidebar('rodller-footer-column-sidebar-4') || is_active_sidebar('rodller-footer-column-sidebar-5'))): ?>
	<div class="container">

		<footer id="rodller-main-footer">
			<div class="row">
				<?php foreach($footer_columns as $column_key => $column_width) :?>
					<?php $column_counter = $column_key + 1; ?>
					<div class="col-md-<?php echo esc_attr($column_width); ?> col-sm-12">
						<?php if(is_active_sidebar('rodller-footer-column-sidebar-' . $column_counter )): ?>
							<?php dynamic_sidebar( 'rodller-footer-column-sidebar-' . $column_counter );?>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
		</footer><!-- #colophon -->
<?php endif; ?>
		<div id="rodller-copyright">
			<div class="row">
				<div class="col-md-4 col-sm-12">
					<?php wp_nav_menu( array( 'theme_location' => 'social-menu' ) ); ?>
				</div>
				<div class="col-md-8 col-sm-12">
					<?php $copyright_bar = rodller_get_option('footer_copyright_bar')  ?>
					<?php $copyright_bar_content = rodller_get_option('footer_copyright_content')  ?>
					<?php if($copyright_bar && !empty($copyright_bar_content)): ?>
						<div>
							<?php echo wp_kses_post(wpautop(do_shortcode($copyright_bar_content))); ?>
						</div>
					<?php endif; ?>					
				</div>
			</div>
		</div>
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>