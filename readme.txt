== Change Log ==

= 1.0 =
* Initial release

= 1.1 =
* SVG upload
* Small CSS fixes
* Paralax added
* AOS animations added

= 1.2 =
* Font size bold as default